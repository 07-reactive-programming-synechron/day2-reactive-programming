package com.classpath.day2reactive.client;

import com.classpath.day2reactive.publisher.Publishers;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import java.time.Duration;

public class ReactorDemo {

    @Test
    public void testMono() {
        Mono<String> monoString = Mono.just("value");
        monoString.subscribe(value -> System.out.println(value));
        StepVerifier
                .create(monoString)
                .expectNext("value")
                .expectComplete()
                .verify();

    }

    @Test
    public void testFluxWithLog(){
        Flux<String> fluxWithValue = Flux.just("one", "two", "three");
        fluxWithValue
                .log()
                .subscribe(String::toUpperCase, (error)-> System.out.println(error.getMessage()), ()-> System.out.println("Completed the evenet"));
    }

    @Test
    public void testCombineOperators() {
        Flux<Integer> flux1 =  Flux.range(3, 5);
        Flux<Integer> flux2 = Flux.range( 30, 5);

        Flux<Integer> concatedFlux = Flux.concat(flux1, flux2);
//                .subscribe((data) -> System.out.println(data));

        StepVerifier.create(concatedFlux)
                .expectNext(3,4,5,6,7,30,31,32,33,34)
                .expectComplete()
                .verify();
    }

    @Test
    public void testWithErrorHandling(){
        Flux<Integer> integerFlux = Flux.just(1,2,3,4);
        Flux<Integer> integerFluxWithException = integerFlux.concatWith(Flux.error(new RuntimeException("Exception while generating a number")));

        integerFluxWithException.subscribe((data) -> System.out.println(data), (error) -> System.out.println("Error "+ error.getMessage()), () -> System.out.println("Completed should not be called"));

        StepVerifier
                .create(integerFluxWithException)
                .expectNext(1,2,3,4)
                .expectError(RuntimeException.class)
                .verify();
    }

    @Test
    public void testWithErrorHandlingWithLog(){
        Flux<Integer> integerFlux = Flux.just(1,2,3,4);
        Flux<Integer> integerFluxWithException = integerFlux.concatWith(Flux.error(new RuntimeException("Exception while generating a number")));
       // integerFluxWithException.subscribe((data) -> System.out.println(data), (error) -> System.out.println("Error "+ error.getMessage()), () -> System.out.println("Completed should not be called"));
        StepVerifier
                .create(integerFluxWithException.log())
                .expectSubscription()
                .expectNext(1,2,3,4)
                .expectError(RuntimeException.class)
                .verify();
    }
    @Test
    public void testWithErrorWithLog(){
        Flux<Integer> integerFlux = Flux.just(1,2,3,4);
        Flux<Integer> integerFluxWithException = integerFlux.concatWith(Flux.error(new RuntimeException("Exception while generating a number")));
        integerFluxWithException.concatWith(Flux.just(5));
       // integerFluxWithException.subscribe((data) -> System.out.println(data), (error) -> System.out.println("Error "+ error.getMessage()), () -> System.out.println("Completed should not be called"));
        StepVerifier
                .create(integerFluxWithException.log())
                .expectSubscription()
                .expectNext(1,2,3,4)
                .expectError(RuntimeException.class)
                .verify();
    }

    @Test
    public void testExceptionHandling(){

        Flux<Integer> integerFlux = Flux.just(1,2,3,4)
                                        .concatWith(Flux.error(new RuntimeException("Exception while generating a number")))
                                        .concatWith(Flux.just(6))
                                        .onErrorResume(e -> {
                                            System.out.println("Error "+ e);
                                            return Flux.empty();
                                        });

        integerFlux.log().subscribe(data -> System.out.println(data));

        StepVerifier
                .create(integerFlux.log())
                .expectSubscription()
                .expectNext(1,2,3,4)
                .expectComplete()
                .verify();
    }

    @Test
    public void testOnErrorReturn(){

        Flux<Integer> integerFlux = Flux.just(1,2,3,4)
                                        .concatWith(Flux.error(new RuntimeException("Exception while generating a number")))
                                        .concatWith(Flux.just(6))
                                        .onErrorReturn(8);

        integerFlux.log().subscribe(data -> System.out.println(data));

        StepVerifier
                .create(integerFlux.log())
                .expectSubscription()
                .expectNext(1,2,3,4)
                .expectNext(8)
                .expectComplete()
                .verify();
    }

    @Test
    public void testDoOnNext() {
        Flux<String> strFlux = Flux.just("hello").doOnNext(value -> {
            System.out.println(" About to induce the delay :: ");
           try {
               Thread.sleep(2000);
               System.out.println(" Delay interval completed ::");
           } catch (InterruptedException exception) {
               exception.printStackTrace();
           }
        });
        strFlux.subscribe(data -> System.out.println(data));
    }

    @Test
    public void testMergeOperator(){

        Flux<Integer> number1 = Flux.range(2,5);
        Flux<Integer> number2 = Flux.range(6,2);

        Flux<Integer> mergedFlux = Flux.merge(number1.delayElements(Duration.ofMillis(2000)), number2.delayElements(Duration.ofMillis(1000)));

        mergedFlux.subscribe(System.out::println);
        try {
            Thread.sleep(8000);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void zipOperatorDemo() {
        Flux<String> firstNames = Flux.just("Ramesh", "Vijay", "Harish");

        Flux<String> lastNames = Flux.just("Kumar", "Mehta", "Rao");

        Flux<String> mergeNames = Flux.zip(firstNames.delayElements(Duration.ofMillis(100)), lastNames.delayElements(Duration.ofMillis(200)), (fName, lName) -> fName + " - " + lName);

        mergeNames.subscribe(data -> System.out.println(data));

        try {
            Thread.sleep(4000);
        } catch(Exception e){
            e.printStackTrace();
        }

    }
    @Test
    public void zipOperatorWithNames() {
        Flux<String> firstNames = Publishers.firstNamePublisher();

        Flux<String> lastNames = Publishers.lastNamePublisher();


        Publishers.mergeNames.subscribe(data -> System.out.println(data));

        try {
            Thread.sleep(4000);
        } catch(Exception e){
            e.printStackTrace();
        }

    }
}