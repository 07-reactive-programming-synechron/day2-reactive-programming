package com.classpath.day2reactive.publisher;

import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;

public class Publishers {

    public static Flux<String> firstNamePublisher(){
        return Flux.fromIterable(List.of("Ramesh", "Vijay", "Harish"))
                                    .concatWith(Flux.error(new RuntimeException("error")))
                                    .onErrorResume(e -> Flux.just("Vinod"))
                                    .delayElements(Duration.ofMillis(100));
    }
    public static Flux<String> lastNamePublisher(){
        return Flux.fromIterable(List.of("Kumar", "Mehta", "Rao", "Mehta")).delayElements(Duration.ofMillis(200));
    }

    public static Flux<String> mergeNames = Flux.zip(firstNamePublisher(), lastNamePublisher(), (fName, lName) -> fName + " - " + lName)
            .onErrorReturn("Exception while concatanating the names");

}